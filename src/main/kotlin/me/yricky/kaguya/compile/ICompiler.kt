package me.yricky.kaguya.compile

import me.yricky.kaguya.base.Module

interface ICompiler {
    fun compile(module: Module):String
}