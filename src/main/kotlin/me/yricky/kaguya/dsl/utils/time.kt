package me.yricky.kaguya.dsl.utils

import me.yricky.kaguya.base.ExpressionWire
import me.yricky.kaguya.base.Signal
import me.yricky.kaguya.dsl.ModuleScope
import me.yricky.kaguya.dsl.SeqLogicScope
import me.yricky.kaguya.dsl.seq

/**
 * 包含时钟和低电平复位的时序逻辑块
 * @param clk 输入的时钟信号
 * @param rstN 输入的复位信号，低电平有效
 */
fun ModuleScope.time(
    clk:Signal,
    rstN:Signal,
    scope:SeqLogicScope.(reset:ExpressionWire)->Unit
){
    seq({
        clk.posEdge()
        rstN.negEdge()
    }){
        scope(this,rstN.inv())
    }
}