package me.yricky.kaguya.dsl.utils

import me.yricky.kaguya.base.NamedSignal
import me.yricky.kaguya.dsl.*

fun ModuleScope.w(name:String, width:Int = 1) = wire(name, width)
fun ModuleScope.ow(name:String,width:Int = 1) = outputWire(name, width)
fun ModuleScope.oReg(name:String,width:Int = 1) = outputReg(name, width)
fun ModuleScope.iw(name:String,width:Int = 1) = inputWire(name, width)

/**
 * 循环右移
 */
context (ModuleScope)
infix fun NamedSignal.rShr(i:Int) = Unit.let {
    if(width == 1){
        this
    }else{
        val sh = i%width
        joint(split(0,sh),split(sh,width-sh))
    }
}

/**
 * 右移，高位填充符号位
 */
context (ModuleScope)
infix fun NamedSignal.shr(i:Int) = Unit.let {
    if(width == 1){
        this
    }else{
        val sh = i%width
        joint(split(width - 1).repeat(sh), split(sh,width-sh))
    }
}

/**
 * 循环左移
 */
context (ModuleScope)
infix fun NamedSignal.rShl(i:Int) = Unit.let {
    if(width == 1){
        this
    }else{
        val sh = i%width
        joint(split(0,width-sh),split(width - sh,sh))
    }
}

/**
 * 左移，低位填充0
 */
context (ModuleScope)
infix fun NamedSignal.shl(i:Int) = Unit.let {
    if(width == 1){
        this
    }else{
        val sh = i%width
        joint(split(0,width-sh), d(0,sh))
    }
}