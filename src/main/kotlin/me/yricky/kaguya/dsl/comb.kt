package me.yricky.kaguya.dsl

import me.yricky.kaguya.base.*
import me.yricky.kaguya.base.lu.AssignBlock
import me.yricky.kaguya.base.lu.CaseFrom
import me.yricky.kaguya.base.lu.CombUnit

class ComLogicScope{
    private val _blocks = mutableListOf<AssignBlock>()
    fun blocks() = _blocks.toList()

    fun Reg.from(
        cases:List<CaseFrom>,
        default:Signal = ConstSignal(width,0)
    ){
        _blocks.add(AssignBlock(this, cases, default))
    }

    fun Reg.from(
        vararg cases: CaseFrom,
        default:Signal = ConstSignal(width,0)
    ) = from(cases.toList(),default)

    fun Reg.from(
        default:Signal
    ) = from(emptyList(),default)

    infix fun Signal.to(that:Signal): CaseFrom {
        assert(width == 1)
        return CaseFrom(this,that)
    }
}

fun ModuleScope.comb(
    logic:ComLogicScope.()->Unit
){
    val logicScope = ComLogicScope().apply(logic)
    _logicUnits.add(
        CombUnit(
        logicScope.blocks()
    )
    )
}