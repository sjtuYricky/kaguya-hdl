package me.yricky.kaguya.base.misc

import me.yricky.kaguya.base.Module
import me.yricky.kaguya.base.lu.ModuleUnit

fun Module.subModules() : List<Module>{
    return sequence {
        yield(this@subModules)
        yieldAll(
            logicUnits.asSequence()
                .mapNotNull { it as? ModuleUnit }
                .flatMap { it.innerModule.subModules() }
        )
    }.toList()
}