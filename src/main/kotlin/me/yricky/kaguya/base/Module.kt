package me.yricky.kaguya.base

import me.yricky.kaguya.base.lu.LogicUnit

/**
 * 模块的抽象，一个module将对应一个Verilog中的module
 *
 * KaguyaHDL中，一个模块由若干信号和若干逻辑构成。
 *
 * @param name
 * @param io
 */
class Module(
    val name:String,
    val io:IO,
    val internalSignal:List<Signal>,
    val logicUnits: List<LogicUnit>
) {
    init {
        // TODO 检查输出信号是否均有数据源
    }

    class IO(
        val inputs:List<NamedWire>,
        val outputs:List<NamedSignal>
    )
}