package me.yricky.kaguya.base

import me.yricky.kaguya.base.lu.ExpressionLogicUnit
import me.yricky.kaguya.base.lu.LogicUnit

sealed interface Signal {
    val width:Int
}

sealed interface Wire:Signal
sealed interface NamedSignal:Signal{
    val name:String
}

class ConstSignal(
    override val width: Int,
    val value:Number,
    val format: Format = Hex
):Signal{
    sealed class Format
    object Hex:Format()
    object Dec:Format()
}

/**
 * 表达式线信号，是一种虚拟的线信号，表示一个可以用表达式表示的信号，例如(a | b)
 */
class ExpressionWire(
    val tag: String,
    override val width: Int,
    val logicUnit: ExpressionLogicUnit,
) :Wire

class NamedWire(override val name: String, override val width: Int) : Wire,NamedSignal{
    var assignedIn: LogicUnit? = null
}
class Reg(override val name: String, override val width: Int): NamedSignal
