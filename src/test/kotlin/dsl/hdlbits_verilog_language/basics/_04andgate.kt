package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module

fun andGate() = module("top_module"){
    (inputWire("a") and inputWire("b")).exportAsOutput("out")
}