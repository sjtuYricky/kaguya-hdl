package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.h
import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.joint
import me.yricky.kaguya.dsl.module

fun vector3() = module("top_module"){
    joint(
        ('a' .. 'f')
            .map { inputWire("$it",5) }
            .plus(h(0b11))
    ).assigned("sig").apply {
        ('w' .. 'z').forEachIndexed { i, c ->
            split((3-i)*8,8).exportAsOutput("$c")
        }
    }
}