package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.joint
import me.yricky.kaguya.dsl.module

fun gates4() = module("top_module"){
    inputWire("in",4).apply {
        and().exportAsOutput("out_and")
        or().exportAsOutput("out_or")
        xor().exportAsOutput("out_xor")
    }
}