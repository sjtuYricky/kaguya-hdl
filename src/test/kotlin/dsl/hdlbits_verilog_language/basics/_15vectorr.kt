package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.joint
import me.yricky.kaguya.dsl.module

fun vectorr() = module("top_module"){
    inputWire("in",8).apply {
        joint((0 until 8).map { split(it) }).exportAsOutput("out")
    }

}