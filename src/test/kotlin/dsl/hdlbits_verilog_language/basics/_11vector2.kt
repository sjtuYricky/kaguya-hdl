package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.joint
import me.yricky.kaguya.dsl.module

fun vector2() = module("top_module"){
    inputWire("in",32).apply {
        joint((0 until 4).map { split(it * 8,8) }).exportAsOutput("out")
    }
}