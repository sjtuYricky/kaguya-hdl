package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module
import me.yricky.kaguya.dsl.outputWire

fun wire4() = module("top_module"){
    val a = inputWire("a")
    val b = inputWire("b")
    val c = inputWire("c")
    outputWire("w").also { it assignTo a }
    outputWire("x").also { it assignTo b }
    outputWire("y").also { it assignTo b }
    outputWire("z").also { it assignTo c }
}