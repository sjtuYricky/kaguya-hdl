package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.joint
import me.yricky.kaguya.dsl.module

fun vector4() = module("top_module"){
    inputWire("in",8).apply {
        joint(split(7).repeat(24),this).exportAsOutput("out")
    }

}