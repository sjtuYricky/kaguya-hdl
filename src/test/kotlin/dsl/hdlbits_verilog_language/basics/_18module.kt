package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.joint
import me.yricky.kaguya.dsl.module
import me.yricky.kaguya.dsl.outputWire

fun module() = module("top_module"){
    module("mod_a"){
        inputWire("in1")
        inputWire("in2")
        outputWire("out")
    }.impl("inst",
        mapOf(
            "in1" to inputWire("a"),
            "in2" to inputWire("b")
        ),
        mapOf(
            "out" to outputWire("out")
        )
    )
}