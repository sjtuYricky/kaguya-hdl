package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module

fun xnorGate() = module("top_module"){
    (inputWire("a") xor inputWire("b")).inv().exportAsOutput("out")
}