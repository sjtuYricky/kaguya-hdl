package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.joint
import me.yricky.kaguya.dsl.module

fun vector5() = module("top_module"){
    val inputs = ('a' .. 'e').map { inputWire("$it") }
    val sig1 = joint(inputs.map { it.repeat(5) })
    val sig2 = joint(inputs).repeat(5)
    (sig1 xor sig2).inv().exportAsOutput("out")
}