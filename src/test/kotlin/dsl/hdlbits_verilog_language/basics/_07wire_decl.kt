package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module

fun wireDecline() = module("top_module"){
    val ab = (inputWire("a") and inputWire("b"))
    val cd = (inputWire("c") and inputWire("d"))
    (ab or cd).apply {
        exportAsOutput("out")
        inv().exportAsOutput("out_n")
    }
}