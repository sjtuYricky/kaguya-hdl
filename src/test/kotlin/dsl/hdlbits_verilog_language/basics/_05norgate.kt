package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module

fun norGate() = module("top_module"){
    (inputWire("a") or inputWire("b")).inv().exportAsOutput("out")
}