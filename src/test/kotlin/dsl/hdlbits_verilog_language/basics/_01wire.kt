package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module
import me.yricky.kaguya.dsl.outputWire

fun wire() = module("top_module"){
    val i = inputWire("in")
    val o = outputWire("out")
    o.assignTo(i)
}