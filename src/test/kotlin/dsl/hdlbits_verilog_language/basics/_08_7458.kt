package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.module
import me.yricky.kaguya.dsl.utils.iw

fun chip7458() = module("top_module"){
    val and31 = (iw("p1a") and iw("p1b") and iw("p1c"))
    val and32 = (iw("p1d") and iw("p1e") and iw("p1f"))
    (and31 or and32).exportAsOutput("p1y")
    ((iw("p2a") and iw("p2b")) or (iw("p2c") and iw("p2d"))).exportAsOutput("p2y")
}