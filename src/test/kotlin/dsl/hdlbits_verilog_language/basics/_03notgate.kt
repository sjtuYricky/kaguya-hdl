package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module

fun notGate() = module("top_module"){
    inputWire("in").inv().exportAsOutput("out")
}