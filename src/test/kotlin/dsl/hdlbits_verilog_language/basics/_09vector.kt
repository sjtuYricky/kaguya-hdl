package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module

fun vector() = module("top_module"){
    inputWire("vec",3).apply {
        (0 until width).forEach {
            split(it).exportAsOutput("o$it")
        }
        assigned("outv").exportAsOutput()
    }
}