package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module

fun vector1() = module("top_module"){
    inputWire("in",16).apply {
        split(0,8).exportAsOutput("out_lo")
        split(8,8).exportAsOutput("out_hi")
    }
}