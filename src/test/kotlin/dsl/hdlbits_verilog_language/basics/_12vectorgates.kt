package dsl.hdlbits_verilog_language.basics

import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.joint
import me.yricky.kaguya.dsl.module

fun vectorGates() = module("top_module"){
    val a = inputWire("a",3)
    val b = inputWire("b",3)
    (a or b).exportAsOutput("out_or_bitwise")
    (a.or() or b.or()).exportAsOutput("out_or_logical")
    joint(b.inv(),a.inv()).exportAsOutput("out_not")
}