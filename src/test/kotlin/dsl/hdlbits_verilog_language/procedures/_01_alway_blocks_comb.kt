package dsl.hdlbits_verilog_language.procedures

import me.yricky.kaguya.dsl.comb
import me.yricky.kaguya.dsl.inputWire
import me.yricky.kaguya.dsl.module
import me.yricky.kaguya.dsl.outputReg

fun alwaysBlocksComb() = module("top_module"){
    val a = inputWire("a")
    val b = inputWire("b")
    (a and b).exportAsOutput("out_assign")
    comb {
        outputReg("out_alwaysblock").from(a and b)
    }
}