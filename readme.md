# KaguyaHDL
[![](https://jitpack.io/v/com.gitee.sjtuYricky/kaguya-hdl.svg)](https://jitpack.io/#com.gitee.sjtuYricky/kaguya-hdl)
> 使用kotlin编写的硬件描述语言

## 设计目标
- 编写构建模块的过程而非直接编写模块
- 尽量用语法进行规范约束，潜在错误左移
- 支持编译到多种HDL语言（优先实现Verilog）
- 产物可综合