plugins {
    kotlin("jvm") version "1.8.21"
    apply {
        id("maven-publish")
    }
}

group = "me.yricky"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}


publishing {
    publications {
        create("maven_public", MavenPublication::class) {
            groupId = "me.yricky.kaguyahdl"
            artifactId = "library"
            version = "1.0.0"
            from(components.getByName("java"))
        }
    }
}


dependencies {
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.named("compileKotlin", org.jetbrains.kotlin.gradle.tasks.KotlinCompilationTask::class.java) {
    compilerOptions {
        freeCompilerArgs.add("-Xcontext-receivers")
    }
}

tasks.named("compileTestKotlin", org.jetbrains.kotlin.gradle.tasks.KotlinCompilationTask::class.java) {
    compilerOptions {
        freeCompilerArgs.add("-Xcontext-receivers")
    }
}

kotlin {
    jvmToolchain(11)
}
